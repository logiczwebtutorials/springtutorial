package org.logiczeeb.tuts.spring;

/**
 * Created by pranav on 3/26/2015.
 */
public class B {
    public B() {
        System.out.println("b is created");
    }

    @Override
    public String toString() {
        return "Class B:  " + super.toString();
    }
}
