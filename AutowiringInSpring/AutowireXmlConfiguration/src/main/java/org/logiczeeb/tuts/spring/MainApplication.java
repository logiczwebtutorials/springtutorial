package org.logiczeeb.tuts.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by pranav on 3/26/2015.
 */
public class MainApplication {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[]{"META-INF/config/byNameApplicationContext.xml"});

        A a = context.getBean("a", A.class);
        System.out.println(a);

        context =
                new ClassPathXmlApplicationContext(new String[]{"META-INF/config/byTypeApplicationContext.xml"});

        a = context.getBean("a", A.class);
        System.out.println(a);


        context =
                new ClassPathXmlApplicationContext(new String[]{"META-INF/config/byConstructorApplicationContext.xml"});
        a = context.getBean("a", A.class);
        System.out.println(a);
    }
}
