/**
 * Created by pranav on 3/26/2015.
 */
package org.logiczeeb.tuts.spring.dependencyinjection;

/*
Problems of Dependency Lookup
    There are mainly two problems of dependency lookup.

    ==>tight coupling The dependency lookup approach makes the code tightly coupled.
    If resource is changed, we need to perform a lot of modification in the code.

    ==>Not easy for testing This approach creates a lot of problems while testing the application
    especially in black box testing.
*/