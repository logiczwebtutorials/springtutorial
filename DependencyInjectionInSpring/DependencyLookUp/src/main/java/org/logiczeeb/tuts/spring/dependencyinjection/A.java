package org.logiczeeb.tuts.spring.dependencyinjection;

/**
 * Created by pranav on 3/26/2015.
 */
public interface A {
    public A getA();
}
