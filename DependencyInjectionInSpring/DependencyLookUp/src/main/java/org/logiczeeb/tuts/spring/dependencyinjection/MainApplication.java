package org.logiczeeb.tuts.spring.dependencyinjection;

import javax.naming.NamingException;

/**
 * Created by pranav on 3/26/2015.
 */
public class MainApplication {

    public static void main(String[] args) throws NamingException {
        A a = new AImpl();
        a.getA();
    }
}
