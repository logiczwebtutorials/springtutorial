package org.logiczeeb.tuts.spring.gettersetter;

import org.logiczeeb.tuts.spring.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by pranav on 3/26/2015.
 */
public class MainApplication {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[]{"META-INF/config/gettersetterApplicationContext.xml"});

        Employee e1 = (Employee) context.getBean("e1");
        e1.display();
    }
}
