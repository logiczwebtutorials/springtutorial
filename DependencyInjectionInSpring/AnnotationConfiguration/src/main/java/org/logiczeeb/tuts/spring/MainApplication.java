package org.logiczeeb.tuts.spring;

import org.logiczeeb.tuts.spring.config.AnnotationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pranav on 3/26/2015.
 */
public class MainApplication {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AnnotationConfiguration.class);

        Employee employee = (Employee) context.getBean("e1");
        employee.display();

        employee = (Employee) context.getBean("e2");
        employee.display();

        employee = (Employee) context.getBean("e3");
        employee.display();

        employee = (Employee) context.getBean("e4");
        employee.display();

        employee = (Employee) context.getBean("e5");
        employee.display();

    }
}
