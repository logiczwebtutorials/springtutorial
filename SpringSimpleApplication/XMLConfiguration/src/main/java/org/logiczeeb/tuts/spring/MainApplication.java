package org.logiczeeb.tuts.spring;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Created by pranav on 3/26/2015.
 */
public class MainApplication {
    public static void main(String[] args) {

        /*Old way, now XmlBeanFactory deprecated*/
        Resource resource = new ClassPathResource("META-INF/config/applicationContext.xml");
        BeanFactory factory = new XmlBeanFactory(resource);

        Student student = (Student) factory.getBean("studentbean");
        student.displayInfo();


        /*Correct way to do it*/
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[]{"META-INF/config/applicationContext.xml"});


        student = (Student) context.getBean("studentbean");
        student.displayInfo();
    }
}
