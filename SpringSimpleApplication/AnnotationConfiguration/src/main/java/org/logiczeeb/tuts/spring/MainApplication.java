package org.logiczeeb.tuts.spring;

import org.logiczeeb.tuts.spring.config.AnnotationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pranav on 3/26/2015.
 */
public class MainApplication {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AnnotationConfiguration.class);
        Student student = (Student) context.getBean("studentbean");
        student.displayInfo();


        context = new AnnotationConfigApplicationContext("org.logiczeeb.tuts.spring.config");
        student = (Student) context.getBean("studentbean");
        student.displayInfo();
    }
}
