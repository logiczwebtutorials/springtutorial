package org.logiczeeb.tuts.spring.config;

import org.logiczeeb.tuts.spring.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pranav on 3/26/2015.
 */
@Configuration
public class AnnotationConfiguration {
    @Bean
    public Student studentbean() {
        Student student = new Student();
        student.setName("John Doe");

        return student;
    }
}
