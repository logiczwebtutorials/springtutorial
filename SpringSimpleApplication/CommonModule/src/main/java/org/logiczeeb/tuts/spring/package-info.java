/**
 * Created by pranav on 3/26/2015.
 */
package org.logiczeeb.tuts.spring;

/*

The IoC container is responsible to instantiate, configure and assemble the objects.
The IoC container gets informations from the XML file and works accordingly.
The main tasks performed by IoC container are:
    ==>to instantiate the application class
    ==>to configure the object
    ==>to assemble the dependencies between the objects

There are 2 types of IoC containers. They are:
    ==>BeanFactory
    ==>ApplicationContext

BeanFactory
    The XmlBeanFactory is the implementation class for the BeanFactory interface.
To use the BeanFactory, we need to create the instance of XmlBeanFactory class as given below:

    Resource resource=new ClassPathResource("applicationContext.xml");
    BeanFactory factory=new XmlBeanFactory(resource);

ApplicationContext
    ClassPathXmlApplicationContext

    The ClassPathXmlApplicationContext class is the implementation class of ApplicationContext interface.
    We need to instantiate the ClassPathXmlApplicationContext class to use the ApplicationContext
    as given below:
    ApplicationContext context =
    new ClassPathXmlApplicationContext("applicationContext.xml");

    AnnotationConfigApplicationContext

    The AnnotationConfigApplicationContext class is the implementation class of ApplicationContext interface.
    We need to instantiate the AnnotationConfigApplicationContext class to use the ApplicationContext
    as given below:

    ==>ApplicationContext context = new AnnotationConfigApplicationContext(A.class);
    ==>ApplicationContext context = new AnnotationConfigApplicationContext("package-name");


*/